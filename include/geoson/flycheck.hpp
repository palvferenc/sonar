#ifndef GEOSON_FLYCHECK
#define GEOSON_FLYCHECK

namespace geoson {

    /**
     * @brief FlyCheck class created only for cmake build testing purpose
     */
    class FlyCheck {
        public:
            int start(int speed) noexcept;
    };
}

#endif