#include <memory>
#include <iostream>

#include <geoson/flycheck.hpp>

int main(int argc, char **argv) {
	auto flycheck = std::make_unique<geoson::FlyCheck>();

	std::cout << flycheck->start(22);

	return 0;
}
