#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <geoson/flycheck.hpp>

TEST_CASE( "Quick check", "[main]" ) {
    
    auto flycheck = geoson::FlyCheck();

    REQUIRE( flycheck.start(22) == 23 );
}
