# GeoSon

UTM coordinate based georeferencing of sonar detection points

## CMake project structure
[Gitlab Knowledge Base](https://cliutils.gitlab.io/modern-cmake/)
[Example project](https://gitlab.com/CLIUtils/modern-cmake/-/tree/master/examples/extended-project)

## CPM CMake dependency manager
[Project site](https://github.com/cpm-cmake/CPM.cmake)
[Blog post](https://medium.com/swlh/cpm-an-awesome-dependency-manager-for-c-with-cmake-3c53f4376766)